from lichess import game
import time
import requests
import json

token = 'kDnFa4yV9KjhYLCp'
url = 'https://lichess.org//api/stream/event'
headers = {'Authorization': 'Bearer ' + token}
r = requests.get(url, headers=headers, stream=True)

for line in r.iter_lines():
    if line:
        getter = json.loads(line.decode('utf-8'))
        #print(getter)
        if getter.get('type') == 'challenge':
            id_game = getter['challenge']['id']
            url = 'https://lichess.org//api/challenge/{}/accept'.format(id_game)
            requests.post(url, headers=headers)
            time.sleep(1)
            game(id_game)
