import chess.engine
import requests
import json


token = 'kDnFa4yV9KjhYLCp'


def game(id_game):
    cc = 0
    count = True
    flag = True
    white = False
    engine = chess.engine.SimpleEngine.popen_uci(
        r"C:\Users\000\PycharmProjects\lichess_bot\stockfish\Windows\stockfish_10_x64")
    board = chess.Board()
    url = 'https://lichess.org//api/bot/game/stream/{}'.format(id_game)
    headers = {'Authorization': 'Bearer ' + token}
    r = requests.get(url, headers=headers, stream=True)
    for line in r.iter_lines():
            if line:
                getter = json.loads(line.decode('utf-8'))

                if flag and getter['white']['id'] == 'telefon2232_bot':
                    white = True
                flag = False
                if getter.get('type') != 'chatLine':
                    if white:
                        if not count:
                            last_move = getter.get('moves').split(' ')[-1]
                            board.push_uci(str(last_move))
                        count = False

                        result = engine.play(board, chess.engine.Limit())
                        best_move = result.move
                        print(board)
                        print('\n')
                        if cc % 2 == 0:
                            url = 'https://lichess.org//api/bot/game/{}/move/{}'.format(id_game, best_move)
                            requests.post(url, headers=headers)

                        cc = cc+1

                    if not white:
                        if count:
                            count = False
                            continue
                        last_move = getter.get('moves').split(' ')[-1]
                        board.push_uci(str(last_move))
                        result = engine.play(board, chess.engine.Limit())
                        best_move = result.move

                        print(board)
                        print('\n')
                        if cc % 2 == 0:
                            url = 'https://lichess.org//api/bot/game/{}/move/{}'.format(id_game, best_move)
                            requests.post(url, headers=headers)

                        cc = cc + 1
                    if cc % 15 == 0:
                        chat(id_game, 'Как же легко, не чувствую тебя')

    board.clear()
    engine.quit()


def chat(id_game, text):
    url = 'https://lichess.org//api/bot/game/{}/chat'.format(id_game)
    payload = {'room': 'player', 'text': text}
    headers = {'Authorization': 'Bearer ' + token, 'content-type': 'application/x-www-form-urlencoded'}
    requests.post(url, headers=headers, data=payload)
